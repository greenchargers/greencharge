<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MarkersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address','text', array(
                'label'=>'adresse'
            ))
//            ->add('adresseligne2')
            ->add('ville')
            ->add('pays')
            ->add('codepostal')
//            ->add('datemodification')

//            ->add('clientid', 'entity', array(
//
//
//                'class' => 'GitesBundle:Clients',
//                'required' => true,
//                'label' => "Client",
//                'property' => 'nom'))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Markers'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gitesbundle_adresses';
    }
}
