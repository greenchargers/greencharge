<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BorneType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('etat')
            ->add('description')
//            ->add('idaddresse')
//            ->add('idclient')
            ->add('idtype')
            ->add('idaddresse', new MarkersType())
            ->add('idtype', 'entity', array(
                    'class' => 'AppBundle\Entity\Typeborne'
                    )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Borne'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_borne';
    }
}
