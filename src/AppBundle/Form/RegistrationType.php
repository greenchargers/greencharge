<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom');
        $builder->add('prenom');
        $builder->add('telephone');
//        $builder->add('titre');
        $builder->add('titre', 'choice', array(
            'choices'  => array(
                'M' => 'M',
                'Mme' => 'Mme',
            ),
//            'expanded'=> true,
//            'multiple' => true,
            // *this line is important*
            'choices_as_values' => true,
        ));

    }

    public function getParent()
    {
        return 'fos_user_registration';

    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}