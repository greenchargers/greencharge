<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

//        $builder->add('titre','choice', array(
//            'choices'  => array(
//                'M' => 'M',
//                'Mme' => 'Mme',
//                           ),
//            'expanded'=> true,
//            'multiple' => true,
//            // *this line is important*
//            'choices_as_values' => true,
//        ));
        $builder->add('titre', 'choice', array(
            'choices'  => array(
                'M' => 'M',
                'Mme' => 'Mme',
                           ),
//            'expanded'=> true,
//            'multiple' => true,
            // *this line is important*
            'choices_as_values' => true,
        ));
        $builder->add('nom');
        $builder->add('prenom');

        $builder->add('telephone');

    //      if(is_granted("ROLE_PROP")) {
//        $builder->add('$proprietaireid','entity', array(
//            'class'=>'GitesBundle\Entity\Proprietaires',
//            'property'=>'langue'
//        ));
////
//          $builder->add('prenom');
//    }
    }

    public function getParent()
    {
        return 'fos_user_profile';

    }

    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}