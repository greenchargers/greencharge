var $collectionTarifs;

// setup an "add a tag" link
var $addTarifsLink = $('<a href="#" class="add_tarifsperiodes_link">Ajouter un tarif</a>');
var $newTarifsLi = $('<li></li>').append($addTarifsLink);

var $collectionImages;

// setup an "add a tag" link
var $addImagesLink = $('<a href="#" class="add_images_link">Ajouter une image</a>');
var $newImagesLi = $('<li></li>').append($addImagesLink);

var $collectionLoisirs;

// setup an "add a tag" link
var $addLoirirsLink = $('<a href="#" class="add_gitesloisirs_link">Ajouter un loisirs</a>');
var $newLoisirsLi = $('<li></li>').append($addLoirirsLink);

jQuery(document).ready(function() {
    // Get the ul that holds the collection of images
    $collectionImages = $('ul.images');


    $collectionImages.find('li').each(function() {
        addTagFormDeleteLink($(this));
    });
    // add the "add a tag" anchor and li to the images ul
    $collectionImages.append($newImagesLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionImages.data('index', $collectionImages.find(':input').length);

    $addImagesLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addTagForm($collectionImages, $newImagesLi);
    });

    $collectionTarifs = $('ul.tarifs');


    $collectionTarifs.find('li').each(function() {
        addTagFormDeleteLink($(this));
    });
    // add the "add a tag" anchor and li to the images ul
    $collectionTarifs.append($newTarifsLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionTarifs.data('index', $collectionTarifs.find(':input').length);

    $addTarifsLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addTagForm($collectionTarifs, $newTarifsLi);
    });


    $collectionLoisirs = $('ul.loisirs');


    $collectionLoisirs.find('li').each(function() {
        addTagFormDeleteLink($(this));
    });
    // add the "add a tag" anchor and li to the images ul
    $collectionLoisirs.append($newLoisirsLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionLoisirs.data('index', $collectionLoisirs.find(':input').length);

    $addLoirirsLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addTagForm($collectionLoisirs, $newLoisirsLi);
    });
});






function addTagForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);
    addTagFormDeleteLink($newFormLi);
}

function addTagFormDeleteLink($tagFormLi) {
    var $removeFormA = $('<a href="#">Supprimer</a>');
    $tagFormLi.append($removeFormA);

    $removeFormA.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // remove the li for the tag form
        $tagFormLi.remove();
    });
}