<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Borne;
use AppBundle\Form\BorneType;
use Ivory\GoogleMap\Map;
use Ivory\GoogleMap\Base\Coordinate;
use Ivory\GoogleMap\Overlay\Marker;
use Ivory\GoogleMap\Event\Event;

use Ivory\GoogleMap\Overlay\Circle;
use Ivory\GoogleMap\Service\Geocoder\Request\GeocoderAddressRequest;

use Ivory\GoogleMap\Service\Geocoder\GeocoderService;
use Http\Adapter\Guzzle6\Client;
use Http\Message\MessageFactory\GuzzleMessageFactory;

/**
 * Borne controller.
 *
 * @Route("/borne")
 */
class BorneController extends Controller
{

    /**
     * Lists all Borne entities.
     *
     * @Route("/", name="borne")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Borne')->findAll();



        $map = new Map();
        $map->setStylesheetOptions( array('position'=>'absolute','width'=>'100%','height'=>'100%','margin'=>'auto','text'=>'center'));
//        $map->setStylesheetOption('width', '70%');
//        $map->setStylesheetOption('height', '70%');
        $map->setAutoZoom(false);
//        $map->setCenter(new Coordinate($lat, $lng));
        $map->setMapOption('zoom', 12);




//// Add a marker to your map
//        $map->getOverlayManager()->addMarker(new Marker(new Coordinate($lat,$lng)));
        foreach ($entities as $borne) {
            if(($borne->getidaddresse()->getLat()!=null) && ($borne->getidaddresse()->getLng()!=null))


                $circle = new Circle(new Coordinate($borne->getidaddresse()->getlat(),$borne->getidaddresse()->getLng()));
            $circle->setOption('clickable', true);
            $circle->setOption('fillColor', '#FFCC75');
            $circle->setOption('fillOpacity', 0.3);
            $circle->setRadius(200);

            $route=$this->generateUrl('borne_show', array('id' => $borne->getIdborne()));
            $function= 'function(){document.location.href=\''.$route.'\';}';
//var_dump( $function);
//            die();
            $event = new Event(
                $circle->getVariable(),
                'click',
                $function,
                true
            );



            $map->getEventManager()->addEvent($event);

            $map->getOverlayManager()->addCircle($circle);

        }

        return array(
            'map'=>$map,
            'entities'=>$entities

//            'entities' => $entities,
        );

        return array(
            'map'=>$map,
            'entities' => $entities,
        );
    }
    /**
     * Lists all Borne entities.
     *
     * @Route("/{lat}/{lng}", name="borne_autour")
     * @Method("GET")
     * @Template("AppBundle:Borne:index.html.twig")
     */
    public function indexfilterAction($lat,$lng)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Borne')->findAll();


        $map = new Map();
        $map->setStylesheetOptions( array('position'=>'absolute','width'=>'100%','height'=>'100%','margin'=>'auto','text'=>'center'));
//        $map->setStylesheetOption('width', '70%');
//        $map->setStylesheetOption('height', '70%');
        $map->setAutoZoom(false);
        $map->setCenter(new Coordinate($lat, $lng));
        $map->setMapOption('zoom', 12);




//// Add a marker to your map
        $map->getOverlayManager()->addMarker(new Marker(new Coordinate($lat,$lng)));
        foreach ($entities as $borne) {
            if(($borne->getidaddresse()->getLat()!=null) && ($borne->getidaddresse()->getLng()!=null))


            $circle = new Circle(new Coordinate($borne->getidaddresse()->getlat(),$borne->getidaddresse()->getLng()));
            $circle->setOption('clickable', true);
            $circle->setOption('fillColor', '#FFCC75');
            $circle->setOption('fillOpacity', 0.3);
            $circle->setRadius(200);

            $route=$this->generateUrl('borne_show', array('id' => $borne->getIdborne()));
            $function= 'function(){document.location.href=\''.$route.'\';}';
//var_dump( $function);
//            die();
            $event = new Event(
                $circle->getVariable(),
                'click',
                $function,
                true
            );
           


            $map->getEventManager()->addEvent($event);

            $map->getOverlayManager()->addCircle($circle);

        }

        return array(
            'map'=>$map,
            'entities'=>$entities,
             'lat'=>$lat,
             'lng'=>$lng
//            'entities' => $entities,
        );

        return array(
            'map'=>$map,
            'entities' => $entities,
        );
    }

    /**
     * Lists all Borne entities.
     *
     * @Route("/recherche", name="borne_recherche")
     * @Method("GET")
     * @Template()
     */
    public function rechercheAction()
    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entities = $em->getRepository('AppBundle:Borne')->findAll();
//
//
//        $map = new Map();
//        $map->setAutoZoom(true);
//        if($request!=null)
//        {
//            $position = $request->get('position');
//            $map->setCenter(new Coordinate(0, 0));
//        }
//
//
//
//
////// Add a marker to your map
//        foreach ($entities as $borne) {
//            if(($borne->getidaddresse()->getLat()!=null) && ($borne->getidaddresse()->getLng()!=null))
//                $map->getOverlayManager()->addMarker(new Marker(new Coordinate($borne->getidaddresse()->getlat(),$borne->getidaddresse()->getLng())));
//        }
//
//        return array(
////            'map'=>$map,
////            'entities'=>$entities
////            'entities' => $entities,
//        );

        return array(
//            'map'=>$map,
//            'entities' => $entities,
        );
    }


    /**
     * Lists all Gites entities.
     *
     * @Route("/client", name="borne_client")
     * @Method("GET")
     * @Template()
     */
    public function indexProprietairesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $prop=$this->getUser();
        $entities = $em->getRepository('AppBundle:Borne')->findBy(array('idclient'=>$prop));
//        $entities = $this->get('knp_paginator')->paginate($fullentities, $this->get('request')->query->get('page', 1), 5);
        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Borne entity.
     *
     * @Route("/", name="borne_create")
     * @Method("POST")
     * @Template("AppBundle:Borne:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Borne();
        $prop = $this->getUser();
        $prop->setRoles(array("ROLE_BORNE"));
        $entity->setIdclient($prop);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $json = file_get_contents('http://maps.google.com/maps/api/geocode/json?address=' . urlencode($entity->getidaddresse()->getCodepostal()) . '+' . urlencode($entity->getidaddresse()->getAddress()) . '+' . urlencode($entity->getidaddresse()->getVille()) . '+' . urlencode($entity->getidaddresse()->getPays()) . '&sensor=false');

            $data = json_decode($json, true);
            if ($data['status'] != 'ZERO_RESULTS') {
                $latitude = $data['results'][0]['geometry']['location']['lat'];
                $longitude = $data['results'][0]['geometry']['location']['lng'];
                $entity->getidaddresse()->setLat($latitude);
                $entity->getidaddresse()->setLng($longitude);
                $em->persist($entity);
                $em->flush();

                return $this->redirect($this->generateUrl('borne_client', array('id' => $entity->getIdborne())));
            } else
                return $this->redirect('borne_new', array('message' => 'Adresse Introuvable'));

            return array(
                'entity' => $entity,
                'form' => $form->createView(),
            );
        }
    }

//    public function createAction(Request $request)
//    {
//        $entity = new Gites();
//        $prop= $this->getUser()->getProprietaire();
//        $entity->setProprietaireid($prop);
//        $form = $this->createCreateForm($entity);
//        $form->handleRequest($request);
//
//        if ($form->isValid()) {
////           $equipements= $entity->getEquipementid();
////            foreach($equipements as $equipement)
////            {
////                $equipement->addGiteid($entity);
////
////            }
//
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($entity);
//            $em->flush();
//
//            return $this->redirect($this->generateUrl('gitesproprietaires'));
//        }
//
//        return array(
//            'entity' => $entity,
//            'form' => $form->createView(),
//        );
//    }


    /**
     * Creates a form to create a Borne entity.
     *
     * @param Borne $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Borne $entity)
    {
        $form = $this->createForm(new BorneType(), $entity, array(
            'action' => $this->generateUrl('borne_create'),
            'method' => 'POST',
        ));

//        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Borne entity.
     *
     * @Route("/new", name="borne_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Borne();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Borne entity.
     *
     * @Route("/{id}", name="borne_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Borne')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Borne entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Borne entity.
     *
     * @Route("/{id}/edit", name="borne_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Borne')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Borne entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Borne entity.
    *
    * @param Borne $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Borne $entity)
    {
        $form = $this->createForm(new BorneType(), $entity, array(
            'action' => $this->generateUrl('borne_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Borne entity.
     *
     * @Route("/{id}", name="borne_update")
     * @Method("PUT")
     * @Template("AppBundle:Borne:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Borne')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Borne entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('borne_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Borne entity.
     *
     * @Route("/{id}", name="borne_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Borne')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Borne entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('borne'));
    }

    /**
     * Creates a form to delete a Borne entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('borne_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
