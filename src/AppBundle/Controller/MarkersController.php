<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Markers;
use AppBundle\Form\MarkersType;
use Ivory\GoogleMap\Map;
use Ivory\GoogleMap\Base\Coordinate;
use Ivory\GoogleMap\Overlay\Marker;
use Ivory\GoogleMap\Service\Geocoder\Request\GeocoderAddressRequest;

use Ivory\GoogleMap\Service\Geocoder\GeocoderService;
use Http\Adapter\Guzzle6\Client;
use Http\Message\MessageFactory\GuzzleMessageFactory;

/**
 * Markers controller.
 *
 * 
 */
class MarkersController extends Controller
{

    /**
     * Lists all Markers entities.
     *
     * @Route("/markers", name="markers")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Markers')->findAll();
        $map = new Map();
        $map->setAutoZoom(true);
//
//// Add a marker to your map
        foreach ($entities as $marker) {
            $map->getOverlayManager()->addMarker(new Marker(new Coordinate($marker->getLat(), $marker->getLng())));
        }
        $map->setMapOption('zoom', 10);

        return array(
            'map'=>$map,
            'entities'=>$entities
//            'entities' => $entities,
        );
    }
    /**
     * Creates a new Markers entity.
     *
     * @Route("/client/adresses", name="adressesclient_create")
     * @Method("POST")
     * @Template("AppBundle:Markers:new.html.twig")
     */
    public function createMarkerscLientsAction(Request $request)
    { $user=$this->getUser();
        $entity = new Markers();
        $entity->addClientid($user);
        $user->addMarkersid($entity);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $request = $entity->getAddress().', '.$entity->getCodepostal().', '.$entity->getVille().','.$entity->getPays();
            $response = $this->container->get('ivory.google_map.geocoder')->geocode($request);

            if($response!=null)
            {

            }
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('adresses_show_by_profile', array('id' => $entity->getid())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }


    /**
     * Creates a new Markers entity.
     *
     * @Route("/markers", name="markers_create")
     * @Method("POST")
     * @Template("AppBundle:Markers:new.html.twig")
     */
    public function createMarkers(Request $request)
    {

        $entity = new Markers();
//        $entity->addClientid($user);
//        $user->addMarkersid($entity);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
//            $geocoder = new GeocoderService(new Client(), new GuzzleMessageFactory());
//            $requete = new GeocoderAddressRequest ($entity->getAddress().', '.$entity->getCodepostal().', '.$entity->getVille().','.$entity->getPays());
////            $response = $this->container->get('ivory.google_map.geocoder')->geocode($requete);
//            $response = $geocoder->geocode($requete);
//
//


            $json = file_get_contents('http://maps.google.com/maps/api/geocode/json?address=' . urlencode($entity->getCodepostal()) . '+' . urlencode($entity->getAddress()) . '+' . urlencode($entity->getVille()) . '+' . urlencode($entity->getPays()) . '&sensor=false');

            $data = json_decode($json, true);
            if ($data['status'] != 'ZERO_RESULTS') {
                $latitude = $data['results'][0]['geometry']['location']['lat'];
                $longitude = $data['results'][0]['geometry']['location']['lng'];
                $entity->setLat($latitude);
                $entity->setLng($longitude);

//            die();


                $em = $this->getDoctrine()->getManager();
//            $request = '1600 Amphitheatre Parkway, Mountain View, CA';
//            $response = $this->container->get('ivory.google_map.geocoder')->geocode($request);


                $em->persist($entity);
                $em->flush();

                return $this->redirect($this->generateUrl('borne', array('id' => $entity->getid())));
            }
            else
                return $this->redirect('borne_new', array('message' => 'Adresse Introuvable'));

        }
    }

    /**
     * Creates a fake Markers entity.
     *
     * @Route("/test/markers", name="fake_markers_create")
     * @Method("POST")
     * @Template("AppBundle:Markers:new.html.twig")
     */
    public function createFakeMarkers(Request $request)
    {

        $entity = new Markers();
//        $entity->addClientid($user);
//        $user->addMarkersid($entity);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
//            $geocoder = new GeocoderService(new Client(), new GuzzleMessageFactory());
//            $requete = new GeocoderAddressRequest ($entity->getAddress().', '.$entity->getCodepostal().', '.$entity->getVille().','.$entity->getPays());
////            $response = $this->container->get('ivory.google_map.geocoder')->geocode($requete);
//            $response = $geocoder->geocode($requete);
//
//


            $json = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($entity->getCodepostal()).'+'.urlencode($entity->getAddress()).'+'.urlencode($entity->getVille()).'+'.urlencode($entity->getPays()).'&sensor=false');



            $data = json_decode($json, true);
                if($data['status']!='ZERO_RESULTS'){

            $latitude = $data['results'][0]['geometry']['location']['lat'];
            $longitude = $data['results'][0]['geometry']['location']['lng'];
//            $entity->setLat($latitude);
//            $entity->setLng($longitude);



                return $this->redirect($this->generateUrl('borne_autour',array('lat'=>$latitude,'lng'=>$longitude)));

            } else return $this->redirect($this->generateUrl('borne'));
}

////            die();
//
//
//
//            $em = $this->getDoctrine()->getManager();
////            $request = '1600 Amphitheatre Parkway, Mountain View, CA';
////            $response = $this->container->get('ivory.google_map.geocoder')->geocode($request);
//
//
//            $em->persist($entity);
//            $em->flush();
//
//            return $this->redirect($this->generateUrl('borne', array('id' => $entity->getid())));
//        }
//
//        return array(
//            'entity' => $entity,
//            'form' => $form->createView(),
//        );
    }

    /**
     * Creates a form to create a Markers entity.
     *
     * @param Markers $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Markers $entity)
    {
        $form = $this->createForm(new MarkersType(), $entity, array(
            'action' => $this->generateUrl('fake_markers_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }
    /**
     * Creates a form to create a Markers entity.
     *
     * @param Markers $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFakeCreateForm(Markers $entity)
    {
        $form = $this->createForm(new MarkersType(), $entity, array(
            'action' => $this->generateUrl('markers_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Markers entity.
     *
     * @Route("/markers/new", name="adresses_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {

        $entity = new Markers();

        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Markers entity.
     *
     * @Route("/markers/new/test", name="adresses_new_test")
     * @Method("GET")
     * @Template("AppBundle:Markers:new.test.html.twig")
     */
    public function newActionTest()
    {

        $entity = new Markers();

        $form = $this->createFakeCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

//    /**
//     * Finds and displays a Markers entity.
//     *
//     * @Route("/{id}", name="adresses_show")
//     * @Method("GET")
//     * @Template()
//     */
//    public function showAction($id)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entity = $em->getRepository('AppBundle:Markers')->find($id);
//
//        if (!$entity) {
//            throw $this->createNotFoundException('Unable to find Markers entity.');
//        }
//
//        $deleteForm = $this->createDeleteForm($id);
//
//        return array(
//            'entity'      => $entity,
//            'delete_form' => $deleteForm->createView(),
//        );
//    }
    /**
     * Finds and displays a Markers .
     *
     * @Route("/client/adresses", name="adresses_show_by_profile")
     * @Method("GET")
     * @Template("AppBundle:Markers:show.html.twig")
     */
    public function showAction2()
    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entities = $em->getRepository('AppBundle:Markers')->findBy(array('clientid'=> $id));
        $entities = $this->getUser()->getid();
        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Markers entity.');
        }
//        $a=1;

        return array(
            'entities' => $entities,

        );
    }

    /**
     * Displays a form to edit an existing Markers entity.
     *
     * @Route("/client/adresses/{id}/edit", name="adresses_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Markers')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Markers entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Markers entity.
     *
     * @param Markers $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Markers $entity)
    {
        $form = $this->createForm(new MarkersType(), $entity, array(
            'action' => $this->generateUrl('adresses_update', array('id' => $entity->getid())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour'));

        return $form;
    }

    /**
     * Edits an existing Markers entity.
     *
     * @Route("/client/adresses/{id}", name="adresses_update")
     * @Method("PUT")
     * @Template("AppBundle:Markers:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Markers')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Markers entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('adresses_show_by_profile'));
        }

        return $this->redirect($this->generateUrl('adresses_show_by_profile'));
//        return array(
//            'entity'      => $entity,
//            'edit_form'   => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
//        );
    }

    /**
     * Deletes a Markers entity.
     *
     * @Route("/client/adresses/{id}", name="adresses_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Markers')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Markers entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('adresses_show_by_profile'));
    }

    /**
     * Creates a form to delete a Markers entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('adresses_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer'))
            ->getForm();
    }
}
