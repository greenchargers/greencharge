<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Creneau;
use AppBundle\Form\CreneauType;
use Symfony\Component\Validator\Constraints\DateTime;


/**
 * Creneau controller.
 *
 *
 */
class CreneauController extends Controller
{

    /**
     * Lists all Creneau entities.
     *
     * @Route("/creneau/{id}/borne", name="creneau")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($id)
    {
        $request = $this->get('request');
//        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Creneau')->findBy(array('idborne'=>$id,'etat'=> true));


        return array(
            'id' => $id,
            'entities' => $entities,
        );
    }

        /**
         * @Route("/creneau/{id}/borne/new/create", name="creneau_create")
         */
        public function createAction()
    {
        // Obtention de l'objet "request"
        $request = $this->get('request');
//         Si l'utilisateur soumet le formulaire
            $manager = $this->getDoctrine()->getManager();
        if ($request->getMethod() == 'POST')
        {
//             Récupération de l'ID du employee à supprimer
            $id = $request->request->get('id');
            $datestart = $request->request->get('datestart');
            $timestart = $request->request->get('timestart');
            $datesend = $request->request->get('datesend');
            $prix = $request->request->get('prix');

            $timeend = $request->request->get('timeend');

            $datestart = str_replace("/", "-", $datestart);
            $datesend = str_replace("/", "-", $datesend);
            $datedebut = new \DateTime($datestart.'T'.$timestart);
            $datefin = new \DateTime($datesend.'T'.$timeend);

            $creneau =new Creneau();
            $entity = $manager->getRepository('AppBundle:Borne')->findBy(array('idborne'=>$id));
            $creneau->setIdborne($entity[0]);

            $creneau->setPrix($prix);
            $creneau->setEtat(true);
            $creneau->setDatedebut($datedebut);
            $creneau->setDatefin($datefin);

            // Obtention du manager
            $manager = $this->getDoctrine()->getManager();


            $message = sprintf("Le employee num %u a ete supprime", $id);
            $status = 0;
            // Suppression du employee
            try
            {
//
                $manager->persist($creneau);
                $manager->flush($creneau);

            }
            catch (\Exception $e)
            {
                $message = sprintf("L erreur suivante est survenue lors de la suppression du employee num %u : %s",
                    $id, $e->getMessage());
                $status = -1;
            }
        }
        else
        {
            $message = "L'appel à la méthode de suppression est incorrecte";
            $status = $id = -1;
        }


        // Retour du résultat en Json
        $response = new Response(json_encode(array('status' => $status, 'message' => $message, 'id' => $id)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

//        $entity = new Creneau();
//        $form = $this->createCreateForm($entity);
//        $form->handleRequest($request);
//
//        if ($form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($entity);
//            $em->flush();
//
//            return $this->redirect($this->generateUrl('creneau_show', array('id' => $entity->getId())));
//        }
//
//        return array(
//            'entity' => $entity,
//            'form'   => $form->createView(),
//        );



    /**
     * Creates a form to create a Creneau entity.
     *
     * @param Creneau $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Creneau $entity)
    {
        $form = $this->createForm(new CreneauType(), $entity, array(
            'action' => $this->generateUrl('creneau_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Creneau entity.
     *
     * @Route("/creneau/{id}/borne/new", name="creneau_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($id)
    {
//        $entity = new Creneau();
//        $form   = $this->createCreateForm($entity);

        return array(
            'id'=>$id
        );
    }

    /**
     * Finds and displays a Creneau entity.
     *
     * @Route("/creneau/{id}", name="creneau_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Creneau')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Creneau entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Creneau entity.
     *
     * @Route("/{id}/edit", name="creneau_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Creneau')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Creneau entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Creneau entity.
    *
    * @param Creneau $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Creneau $entity)
    {
        $form = $this->createForm(new CreneauType(), $entity, array(
            'action' => $this->generateUrl('creneau_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Creneau entity.
     *
     * @Route("/{id}", name="creneau_update")
     * @Method("PUT")
     * @Template("AppBundle:Creneau:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Creneau')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Creneau entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('creneau_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Creneau entity.
     *
     * @Route("/{id}", name="creneau_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Creneau')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Creneau entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('creneau'));
    }


    /**
     * Creates a form to delete a Creneau entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('creneau_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
