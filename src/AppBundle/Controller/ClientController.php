<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GitesBundle\Entity\Client;
use GitesBundle\Form\ClientType;

/**
 * Client controller.
 *
 * @Route("/client")
 */
class ClientController extends Controller
{
//
//    /**
//     * Lists all Client entities.
//     *
//     * @Route("/", name="clients")
//     * @Method("GET")
//     * @Template()
//     */
//    public function indexAction()
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entities = $em->getRepository('GitesBundle:Client')->findAll();
//
//        return array(
//            'entities' => $entities,
//        );
//    }
//
    /**
     * Lists all Client entities.
     *
     * @Route("/menu", name="clientsmenu")
     * @Method("GET")
    @Template("AppBundle:Clients:menuclients.html.twig")
     */
    public function MenuAction()
    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entities = $em->getRepository('GitesBundle:Client')->findAll();
        $user = $this->getUser();


        return array(
            'user' => $user,
        );
    }

    /**
     * Lists all Client entities.
     *
     * @Route("/profil", name="clientsmenu_profil")
     * @Method("GET")
    @Template("AppBundle:Clients:menuclients.profil.html.twig")
     */
    public function MenuClientAction()
    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entities = $em->getRepository('GitesBundle:Client')->findAll();
        $user = $this->getUser();


        return array(
            'user' => $user,
        );
    }


    /**
     * Lists all Gites entities.
     *
     * @Route("/borne", name="borne_client")
     * @Method("GET")
     * @Template("AppBundle:Borne:index.client.html.twig")
     */
    public function indexProprietairesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $prop=$this->getUser();
        $entities = $em->getRepository('AppBundle:Borne')->findBy(array('idclient'=>$prop));
//        $entities = $this->get('knp_paginator')->paginate($fullentities, $this->get('request')->query->get('page', 1), 5);
        return array(
            'entities' => $entities,
        );
    }

}
