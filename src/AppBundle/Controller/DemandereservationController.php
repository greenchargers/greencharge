<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Demandereservation;
use AppBundle\Form\DemandereservationType;

/**
 * Demandereservation controller.
 *
 * @Route("/client/demandereservation")
 */
class DemandereservationController extends Controller
{

    /**
     * Lists all Demandereservation entities.
     *
     * @Route("/borne/{id}", name="demandereservation_borne")
     * @Method("GET")
     * @Template("AppBundle:Demandereservation:index.borne.html.twig")
     */
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user=$this->getUser();
        $entities = $em->getRepository('AppBundle:Demandereservation')->finddemandereservation($id,$user);

        return array(
            'entities' => $entities,
        );
    }/**
     * Lists all Demandereservation entities.
     *
     * @Route("/", name="demandereservation_client")
     * @Method("GET")
     * @Template("AppBundle:Demandereservation:index.html.twig")
     */
    public function indexAction2()
    {
        $em = $this->getDoctrine()->getManager();
        $user=$this->getUser();
        $entities = $em->getRepository('AppBundle:Demandereservation')->findBy(array('idclient'=>$user));

        return array(
            'entities' => $entities,
        );
    }

//    /**
//     * Lists all Demandereservation entities.
//     *
//     * @Route("/validation/{id}", name="demandereservation")
//     * @Method("GET")
//     * @Template()
//     */
//    public function validation($id)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $creneau = $em->getRepository('AppBundle:Creneau')->find($id);
//
//        return array(
//            'creneau' => $creneau,
//        );
//    }

    /**
     * Creates a new Demandereservation entity.
     *
     * @Route("/validation/{id}", name="demandereservation_create")
     * @Method("GET")
     * @Template("AppBundle:Demandereservation:confirmation.html.twig")
     */
    public function createAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Demandereservation();
        $creneau=$em->getRepository('AppBundle:Creneau')->find($id);
        if($creneau!=null)
        {
            $creneau->setEtat(false);

//            $entity->addIdcreneau($creneau);
            $entity->setPrixtotal($creneau->getPrix());
            $entity->setStatutpaiement(false);
            $entity->setIdclient($this->getUser());
            $creneau->addIddemandereservation($entity);
            try
            {
                $em->persist($entity);
                $em->persist($creneau);
                $em->flush();

            }
            catch (\Exception $e)
            {

            }
        }


        return array(
//            'entity' => $entity,
//            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Demandereservation entity.
     *
     * @param Demandereservation $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Demandereservation $entity)
    {
        $form = $this->createForm(new DemandereservationType(), $entity, array(
            'action' => $this->generateUrl('demandereservation_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Demandereservation entity.
     *
     * @Route("/new", name="demandereservation_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Demandereservation();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Demandereservation entity.
     *
     * @Route("/{id}", name="demandereservation_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Demandereservation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Demandereservation entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Demandereservation entity.
     *
     * @Route("/{id}/edit", name="demandereservation_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Demandereservation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Demandereservation entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Demandereservation entity.
    *
    * @param Demandereservation $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Demandereservation $entity)
    {
        $form = $this->createForm(new DemandereservationType(), $entity, array(
            'action' => $this->generateUrl('demandereservation_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Demandereservation entity.
     *
     * @Route("/{id}", name="demandereservation_update")
     * @Method("PUT")
     * @Template("AppBundle:Demandereservation:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Demandereservation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Demandereservation entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('demandereservation_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Demandereservation entity.
     *
     * @Route("/{id}", name="demandereservation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Demandereservation')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Demandereservation entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('demandereservation'));
    }

    /**
     * Creates a form to delete a Demandereservation entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('demandereservation_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
