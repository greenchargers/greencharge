<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Creneau
 *
 * @ORM\Table(name="creneau", indexes={@ORM\Index(name="FK_IdBorne_idx", columns={"IdBorne"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CreneauRepository")
 */
class Creneau
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdCreneau", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcreneau;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateDebut", type="datetime", nullable=false)
     */
    private $datedebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateFin", type="datetime", nullable=false)
     */
    private $datefin;

    /**
     * @var string
     *
     * @ORM\Column(name="Prix", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $prix;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Etat", type="boolean", options={"default":true})
     */
    private $etat;

    /**
     * @var \Borne
     *
     * @ORM\ManyToOne(targetEntity="Borne")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdBorne", referencedColumnName="IdBorne")
     * })
     */
    private $idborne;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Demandereservation", mappedBy="idcreneau",cascade={"persist"})
     */
    private $iddemandereservation;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Reservation", mappedBy="idcreneau", cascade={"persist"})
     */
    private $idreservation;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->iddemandereservation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idreservation = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idcreneau
     *
     * @return integer 
     */
    public function getIdcreneau()
    {
        return $this->idcreneau;
    }

    /**
     * Set datedebut
     *
     * @param \DateTime $datedebut
     * @return Creneau
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    /**
     * Get datedebut
     *
     * @return \DateTime 
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set datefin
     *
     * @param \DateTime $datefin
     * @return Creneau
     */
    public function setDatefin($datefin)
    {
        $this->datefin = $datefin;

        return $this;
    }

    /**
     * Get datefin
     *
     * @return \DateTime 
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

    /**
     * Set prix
     *
     * @param string $prix
     * @return Creneau
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return string 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set etat
     *
     * @param boolean $etat
     * @return Creneau
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return boolean 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set idborne
     *
     * @param \AppBundle\Entity\Borne $idborne
     * @return Creneau
     */
    public function setIdborne(\AppBundle\Entity\Borne $idborne = null)
    {
        $this->idborne = $idborne;

        return $this;
    }

    /**
     * Get idborne
     *
     * @return \AppBundle\Entity\Borne 
     */
    public function getIdborne()
    {
        return $this->idborne;
    }

    /**
     * Add iddemandereservation
     *
     * @param \AppBundle\Entity\Demandereservation $iddemandereservation
     * @return Creneau
     */
    public function addIddemandereservation(\AppBundle\Entity\Demandereservation $iddemandereservation)
    {

        $this->iddemandereservation[] = $iddemandereservation;
        $iddemandereservation->getIdcreneau()->add($this);

        return $this;
    }

    /**
     * Remove iddemandereservation
     *
     * @param \AppBundle\Entity\Demandereservation $iddemandereservation
     */
    public function removeIddemandereservation(\AppBundle\Entity\Demandereservation $iddemandereservation)
    {
        $this->iddemandereservation->removeElement($iddemandereservation);
    }

    /**
     * Get iddemandereservation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIddemandereservation()
    {
        return $this->iddemandereservation;
    }

    /**
     * Add idreservation
     *
     * @param \AppBundle\Entity\Reservation $idreservation
     * @return Creneau
     */
    public function addIdreservation(\AppBundle\Entity\Reservation $idreservation)
    {
        $this->idreservation[] = $idreservation;

        return $this;
    }

    /**
     * Remove idreservation
     *
     * @param \AppBundle\Entity\Reservation $idreservation
     */
    public function removeIdreservation(\AppBundle\Entity\Reservation $idreservation)
    {
        $this->idreservation->removeElement($idreservation);
    }

    /**
     * Get idreservation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdreservation()
    {
        return $this->idreservation;
    }
}
