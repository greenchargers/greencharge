<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Typeborne
 *
 * @ORM\Table(name="typeborne")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MarkersRepository")
 */
class Typeborne
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdTypeBorne", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtypeborne;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=45, nullable=true)
     */
    private $nom;



    /**
     * Get idtypeborne
     *
     * @return integer 
     */
    public function getIdtypeborne()
    {
        return $this->idtypeborne;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Typeborne
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * toString
     * @return string
     */
    public function __toString()
    {
        return $this->nom;
    }
}
