<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Markers
 *
 * @ORM\Table(name="markers")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MarkersRepository")
 */
class Markers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

//    /**
//     * @var string
//     *
//     * @ORM\Column(name="name", type="string", length=60, nullable=false)
//     */
//    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=80, nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="Ville", type="string", length=45, nullable=false)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="Pays", type="string", length=45, nullable=false)
     */
    private $pays;

    /**
     * @var integer
     *
     * @ORM\Column(name="CodePostal", type="string", length=50, nullable=false)
     */
    private $codepostal;


    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float", precision=10, scale=6, nullable=true)
     */


    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lng", type="float", precision=10, scale=6, nullable=true)
     */
    private $lng;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Etat", type="boolean", nullable=true)
     */
    private $etat;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Markers
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Markers
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set lat
     *
     * @param float $lat
     * @return Markers
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param float $lng
     * @return Markers
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return float 
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set etat
     *
     * @param boolean $etat
     * @return Markers
     */
    public function setEtat($etat)
    {
        if($etat=!false)
        {
            $this->etat = $etat;
        }
     else  $this->etat = true;

        return $this;
    }

    /**
     * Get etat
     *
     * @return boolean 
     */
    public function getEtat()
    {
        return $this->etat;
    }

//    /**
//     * Set type
//     *
//     * @param string $type
//     * @return Markers
//     */
//    public function setType($type)
//    {
//        $this->type = $type;
//
//        return $this;
//    }
//
//    /**
//     * Get type
//     *
//     * @return string
//     */
//    public function getType()
//    {
//        return $this->type;
//    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Markers
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Markers
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set codepostal
     *
     * @param string $codepostal
     *
     * @return Markers
     */
    public function setCodepostal($codepostal)
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    /**
     * Get codepostal
     *
     * @return string
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }


    /**
     *
     *
     * @ORM\OneToOne(targetEntity="Borne", inversedBy="id", mappedBy="idaddresse",cascade={"persist"})
     * })
     */
    private $borne;

    /**
     * Set borne
     *
     * @param \AppBundle\Entity\Borne $borne
     *
     * @return Markers
     */
    public function setBorne(\AppBundle\Entity\Borne $borne = null)
    {
        $this->borne = $borne;

        return $this;
    }

    /**
     * Get gites
     *
     * @return \AppBundle\Entity\Borne
     */
    public function getBorne()
    {
        return $this->borne;
    }


}
