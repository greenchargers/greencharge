<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Borne
 *
 * @ORM\Table(name="borne", indexes={@ORM\Index(name="FK_IdAdresse_idx", columns={"IdAddresse"}) })
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BorneRepository")
 */
class Borne
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdBorne", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idborne;

//    /**
//     * @var boolean
//     *
//     * @ORM\Column(name="Etat", type="boolean", nullable=false)
//     */
//    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="text", length=16777215, nullable=true)
     */
    private $description;

    /**
     * @var \Markers
     *
     * @ORM\OneToOne(targetEntity="Markers",cascade={"persist","remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdAddresse", referencedColumnName="id")
     * })
     */
    private $idaddresse;


    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Client",inversedBy="images",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdClient", referencedColumnName="IdClient")
     * })
     */
    private $idclient;


    /**
     * @var \Typeborne
     *
     * @ORM\ManyToOne(targetEntity="Typeborne")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdType", referencedColumnName="IdTypeBorne")
     * })
     */
    private $idtype;



    /**
     * Get idborne
     *
     * @return integer 
     */
    public function getIdborne()
    {
        return $this->idborne;
    }

//    /**
//     * Set etat
//     *
//     * @param boolean $etat
//     * @return Borne
//     */
//    public function setEtat($etat)
//    {
//        $this->etat = $etat;
//
//        return $this;
//    }
//
//    /**
//     * Get etat
//     *
//     * @return boolean
//     */
//    public function getEtat()
//    {
//        return $this->etat;
//    }

    /**
     * Set description
     *
     * @param string $description
     * @return Borne
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set Markers
     *
     * @param \AppBundle\Entity\Markers $idaddresse
     * @return Borne
     */
    public function setidaddresse(\AppBundle\Entity\Markers $idaddresse = null)
    {
        $idaddresse->setBorne($this);
//        $this->adresseid = $adresseid;
        $this->idaddresse = $idaddresse;

        return $this;
    }

    /**
     * Get idaddresse
     *
     * @return \AppBundle\Entity\Markers 
     */
    public function getidaddresse()
    {
        return $this->idaddresse;
    }

    /**
     * Set idclient
     *
     * @param \AppBundle\Entity\Client $idclient
     * @return Borne
     */
    public function setIdclient(\AppBundle\Entity\Client $idclient = null)
    {
        $this->idclient = $idclient;

        return $this;
    }

    /**
     * Get idclient
     *
     * @return \AppBundle\Entity\Client 
     */
    public function getIdclient()
    {
        return $this->idclient;
    }

    /**
     * Set idtype
     *
     * @param \AppBundle\Entity\Typeborne $idtype
     * @return Borne
     */
    public function setIdtype(\AppBundle\Entity\Typeborne $idtype = null)
    {
        $this->idtype = $idtype;

        return $this;
    }

    /**
     * Get idtype
     *
     * @return \AppBundle\Entity\Typeborne 
     */
    public function getIdtype()
    {
        return $this->idtype;
    }


//    /**
//     * Set adresseid
//     *
//     * @param \AppBundle\Entity\Markers $adresseid
//     *
//     * @return Borne
//     */
//    public function setAdresseid(\AppBundle\Entity\Markers $adresseid = null)
//    {
//        $adresseid->($this);
//        $this->adresseid = $adresseid;
//
//        return $this;
//    }
//
//    /**
//     * Get adresseid
//     *
//     * @return \AppBundle\Entity\Markers
//     */
//    public function getAdresseid()
//    {
//        return $this->adresseid;
//    }

    /**
     * toString
     * @return string
     */
    public function __toString()
    {
        return $this->nom;
    }
}
