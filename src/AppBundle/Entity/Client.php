<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Client
 *
 * @ORM\Table(name="client", indexes={@ORM\Index(name="FK_IdFacturation_idx", columns={"IdAddresseFacturation"}), @ORM\Index(name="FK_IdAdresse_idx", columns={"IdAddresse"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientRepository")
 */
class Client extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdClient", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected  $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=45, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre", type="string", length=45, nullable=true)
     * @Assert\NotBlank(message="Renseignez votre titre.", groups={"Registration", "Profile"})
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="Prenom", type="string", length=45, nullable=false)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="Telephone", type="string", length=45, nullable=true)
     */
    private $telephone;

//    /**
//     * @var string
//     *
//     * @ORM\Column(name="Email", type="string", length=200, nullable=false)
//     */
//    private $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Etat", type="boolean", nullable=true)
     */
    private $etat;

    /**
     * @var \Markers
     *
     * @ORM\ManyToOne(targetEntity="Markers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdAddresse", referencedColumnName="id")
     * })
     */
    private $idaddresse;

    /**
     * @var \Markers
     *
     * @ORM\ManyToOne(targetEntity="Markers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdAddresseFacturation", referencedColumnName="id")
     * })
     */
    private $idaddressefacturation;



    /**
     * Get idclient
     *
     * @return integer 
     */
    public function getIdclient()
    {
        return $this->idclient;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Client
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Client
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Client
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set etat
     *
     * @param boolean $etat
     * @return Client
     */
    public function setEtat($etat)
    {
        $this->etat = true;

        return $this;
    }

    /**
     * Get etat
     *
     * @return boolean 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set idaddresse
     *
     * @param \AppBundle\Entity\Markers $idaddresse
     * @return Client
     */
    public function setIdaddresse(\AppBundle\Entity\Markers $idaddresse = null)
    {
        $this->idaddresse = $idaddresse;

        return $this;
    }

    /**
     * Get idaddresse
     *
     * @return \AppBundle\Entity\Markers 
     */
    public function getIdaddresse()
    {
        return $this->idaddresse;
    }

    /**
     * Set idaddressefacturation
     *
     * @param \AppBundle\Entity\Markers $idaddressefacturation
     * @return Client
     */
    public function setIdaddressefacturation(\AppBundle\Entity\Markers $idaddressefacturation = null)
    {
        $this->idaddressefacturation = $idaddressefacturation;
        $this->borne = new \Doctrine\Common\Collections\ArrayCollection();

        return $this;
    }

    /**
     * Get idaddressefacturation
     *
     * @return \AppBundle\Entity\Markers 
     */
    public function getIdaddressefacturation()
    {
        return $this->idaddressefacturation;
    }
    public function getParent()
    {
        return 'FOSUserBundle';
    }
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Clients
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Borne", mappedBy="borne", cascade={"persist"}, orphanRemoval=true)
     * })
     */
    private $bornes;

    /**
     * Remove borne
     *
     * @param \AppBundle\Entity\Borne $borne
     */
    public function removeBorne(\AppBundle\Entity\Borne $borne)
    {

        $this->bornes->removeElement($borne);
//        $borne->removeGiteid($this);
    }

    /**
     * Get borne
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getbornes()
    {
        return $this->bornes;
    }
    /**
     * Add gitesLoisir
     *
     * @param \AppBundle\Entity\Borne $borne
     *
     * @return Gites
     */
    public function addBorne(\AppBundle\Entity\Borne $borne)
    {
        $borne->setIdclient($this);
        $this->bornes[] = $borne;

        return $this;
    }
}
