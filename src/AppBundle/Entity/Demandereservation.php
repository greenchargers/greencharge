<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Demandereservation
 *
 * @ORM\Table(name="demandereservation", indexes={@ORM\Index(name="FK_IdClient_idx", columns={"IdClient"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DemandereservationRepository")
 */
class Demandereservation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdDemandeReservation", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iddemandereservation;

    /**
     * @var string
     *
     * @ORM\Column(name="PrixTotal", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $prixtotal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="StatutPaiement", type="boolean", nullable=false)
     */
    private $statutpaiement;

    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdClient", referencedColumnName="IdClient")
     * })
     */
    private $idclient;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Creneau", inversedBy="iddemandereservation", cascade={"persist"})
     * @ORM\JoinTable(name="demandereservationcreneau",
     *   joinColumns={
     *     @ORM\JoinColumn(name="IdDemandeReservation", referencedColumnName="IdDemandeReservation")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="IdCreneau", referencedColumnName="IdCreneau")
     *   }
     * )
     */
    private $idcreneau;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idcreneau = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get iddemandereservation
     *
     * @return integer 
     */
    public function getIddemandereservation()
    {
        return $this->iddemandereservation;
    }

    /**
     * Set prixtotal
     *
     * @param string $prixtotal
     * @return Demandereservation
     */
    public function setPrixtotal($prixtotal)
    {
        $this->prixtotal = $prixtotal;

        return $this;
    }

    /**
     * Get prixtotal
     *
     * @return string 
     */
    public function getPrixtotal()
    {
        return $this->prixtotal;
    }

    /**
     * Set statutpaiement
     *
     * @param boolean $statutpaiement
     * @return Demandereservation
     */
    public function setStatutpaiement($statutpaiement)
    {
        $this->statutpaiement = $statutpaiement;

        return $this;
    }

    /**
     * Get statutpaiement
     *
     * @return boolean 
     */
    public function getStatutpaiement()
    {
        return $this->statutpaiement;
    }

    /**
     * Set idclient
     *
     * @param \AppBundle\Entity\Client $idclient
     * @return Demandereservation
     */
    public function setIdclient(\AppBundle\Entity\Client $idclient = null)
    {
        $this->idclient = $idclient;

        return $this;
    }

    /**
     * Get idclient
     *
     * @return \AppBundle\Entity\Client 
     */
    public function getIdclient()
    {
        return $this->idclient;
    }

    /**
     * Add idcreneau
     *
     * @param \AppBundle\Entity\Creneau $idcreneau
     * @return Demandereservation
     */
    public function addIdcreneau(\AppBundle\Entity\Creneau $idcreneau)
    {
        $this->idcreneau[] = $idcreneau;

        return $this;
    }

    /**
     * Remove idcreneau
     *
     * @param \AppBundle\Entity\Creneau $idcreneau
     */
    public function removeIdcreneau(\AppBundle\Entity\Creneau $idcreneau)
    {
        $this->idcreneau->removeElement($idcreneau);
    }

    /**
     * Get idcreneau
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdcreneau()
    {
        return $this->idcreneau;
    }
}
