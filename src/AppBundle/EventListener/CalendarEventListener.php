<?php
/**
 * Created by PhpStorm.
 * User: Vincent
 * Date: 15/05/2016
 * Time: 21:27
 */

namespace AppBundle\EventListener;

use ADesigns\CalendarBundle\Event\CalendarEvent;
use ADesigns\CalendarBundle\Entity\EventEntity;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CalendarEventListener
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function loadEvents(CalendarEvent $calendarEvent)
    {
        $controller= new Controller();
        $startDate = $calendarEvent->getStartDatetime()->setTime(00, 00);
        $endDate = $calendarEvent->getEndDatetime()->setTime(00, 00);

        // The original request so you can get filters from the calendar
        // Use the filter in your query for example

        $request = $calendarEvent->getRequest();
        $filter = $request->get('filter');


        // load events using your custom logic here,
        // for instance, retrieving events from a repository

        $disponibilites = $this->entityManager->getRepository('AppBundle:Creneau')->bornecreneau($startDate, $endDate, $filter);

//        $dates = array();
//        foreach ($disponibilites as $date) {
//            $date = $date->getDate();
//            array_push($dates, $date);
//        }
//var_dump($tarifsperiodes);
//        die();

//            ->from('AppBundle:Calendrier')
//          ->join('calendrier.periodes')
////            ->join('p.tarifsperiodes', 'tp')
//////            ->where('p.datedebut < :endDate')
//////            ->andWhere('tp.gitesid=:giteid')
////////            ->setParameter('startDate', $startDate->format('Y-m-d H:i:s'))
//////            ->setParameter('endDate', $endDate->format('Y-m-d H:i:s'))
//////            ->setParameter('giteid', $filter)
//            ->getQuery()->getResult();


//            ->createQueryBuilder('u')
//            ->select('u')
//            ->getQuery()->getResult();
//            ->join('u.periodes', 'p')
//            ->where('tp.giteid = :giteid')
//            ->setParameter('giteid', $filter)


        // $companyEvents and $companyEvent in this example
        // represent entities from your database, NOT instances of EventEntity
        // within this bundle.
        //
        // Create EventEntity instances and populate it's properties with data
        // from your own entities/database values.
//
//        $datearrivee=new \Datetime($startDate);
//        $datedepart=new \Datetime($endDate);
//        $dateperiod = new \DateInterval('P1H');
//        $periodes = new \DatePeriod($startDate, $dateperiod, $endDate);


        foreach ($disponibilites as $creneau) {
//            $a=1;
//$date=$date->setTime(00, 00);

                $eventEntity = new EventEntity(
                    $creneau->getPrix(),$creneau->getDatedebut(),$creneau->getDatefin())
            ;

                //optional calendar event settings
                $eventEntity->setAllDay(false); // default is false, set to true if this is an all day event
//                $eventEntity->setStartDatetime($creneau->getDatedebut());
//                $eventEntity->setEndDatetime($creneau->getDatefin());
                $eventEntity->setBgColor('#B2FF6F'); //set the background color of the event's label
                $eventEntity->setFgColor('#FFFFFF'); //set the foreground color of the event's label
            $eventEntity->setUrl('/client/demandereservation/validation/'. $creneau->getIdcreneau()); // url to send user to when event label is clicked
                $eventEntity->setCssClass('my-custom-class'); // a custom class you may want to apply to event labels

                //finally, add the event to the CalendarEvent for displaying on the calendar
                $calendarEvent->addEvent($eventEntity);
            }
//            else {
//                $eventEntity = new EventEntity('', $date, null, true);
//
//
//                //optional calendar event settings
//                $eventEntity->setAllDay(true); // default is false, set to true if this is an all day event
//                $eventEntity->setBgColor('#FF3E23'); //set the background color of the event's label
//                $eventEntity->setFgColor('#FFFFFF'); //set the foreground color of the event's label
////            $eventEntity->setUrl('http://www.google.com'); // url to send user to when eventlabel is clicked
//                $eventEntity->setCssClass('my-custom-class'); // a custom class you may want to apply to event labels
//
//                //finally, add the event to the CalendarEvent for displaying on the calendar
//                $calendarEvent->addEvent($eventEntity);
//            }
        }

}

